# This is a fully functional app I made using **Ionic** framework. #

**Its a simple task tracking app that allows you to track how well you show your Genius traits thru out the day.**  It follows the principle that if you master these traits on a daily basis you become closer to reaching the Genius within you. 

As you progress you can add status to each characteristic and your status is clearly shown in a nice pie chart on the top. All data is saved locally within the app's localstorage and backed up with the app data.

Cloning the entire repo will get you everything including unrelated files you ay not need and might be outdated for building. **All you really need is the www directory and you can preview and build from there.** 

**This app is also hosted on http://view.ionic.io/ with app id: 1245f046**