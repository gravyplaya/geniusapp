// Written by: Geovanni Hudson for Pharmacyclics, Inc.

angular.module('ionicApp', ['ionic', 'kendo.directives'])

.factory('$localstorage', ['$window', function($window) {
    return {
     set: function(key, value) {
     $window.localStorage[key] = value;
     },
     get: function(key, defaultValue) {
     return $window.localStorage[key] || defaultValue;
     },
     setObject: function(key, value) {
     $window.localStorage[key] = JSON.stringify(value);
     },
     getObject: function(key) {
     return JSON.parse($window.localStorage[key] || '{}');
     }
    }
 }])



.run(function($ionicPlatform, $localstorage,  $timeout, $rootScope, $ionicPosition) {
  $ionicPlatform.ready(function() {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    // also lock the top device notification/statusbar for input form focus
    if(window.cordova && window.cordova.plugins.Keyboard) {
      //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    $rootScope.hideSplashScreen = function() {
        $ionicPlatform.ready(function() {
          if(navigator.splashscreen){
            navigator.splashscreen.hide();
          }
         // $cordovaSplashscreen.hide();
       });
      };
      //navigator.splashscreen.hide();
  });


    //this sets the p tags previous to OL
    $timeout(function(){
              $('ol').prev('p').css('margin', '0');
             //determine the content area height
             //var cHeight = $('.has-header').height() || 420 ;
             var pos = $ionicPosition.offset($('.has-header'));
             var cHeight = pos.height ;
             var cloner = parseInt(cHeight - 13);
             var frost = parseInt(cHeight - 23);
             var scroll = parseInt(cHeight - 43);
             var notesHeight = parseInt(cHeight - 266);
             var notesHistHeight = parseInt(cHeight - 96);

             //$('.cloner').css('height', cloner);
             //$('.frost').css('height', frost);
             //$('.notes').css('height', notesHeight);
             //$('.noteshistory').css('height', notesHistHeight);

             $rootScope.cHeight = cHeight;
             $rootScope.cloner = cHeight;
             $rootScope.frost = frost;
             $rootScope.scroll = scroll;
             $rootScope.notesHeight = notesHeight;
             $rootScope.notesHistHeight = notesHistHeight;

    }, 200);




})



.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  //$ionicConfigProvider.backButton.text('');
  $ionicConfigProvider.backButton.previousTitleText(false).text('').icon('ion-arrow-left-c');
//  $ionicConfigProvider.views.forwardCache(true);


        $stateProvider
        .state('terms', {
               url: "/terms",
               templateUrl: "templates/terms.html",
               controller: "termsCtrl"
             })
        .state('genius', {
               url: "/genius",
               abstract: true,
               templateUrl: "templates/base.html"
               })
        .state('genius.home', {
               url: "/home",
               views: {
               'menuContent' :{
               templateUrl: "templates/home.html"
               }
               }
               })
        .state('genius.genius', {
               url: "/genius",
               views: {
               'menuContent' :{
               templateUrl: "templates/genius.html"
               }
               }
               })
        .state('genius.drive', {
               url: "/drive",
               views: {
               'menuContent' :{
               templateUrl: "templates/drive.html"
               }
               }
               })
        .state('genius.courage', {
               url: "/courage",
               views: {
               'menuContent' :{
               templateUrl: "templates/courage.html"
               }
               }
               })
        .state('genius.devotion', {
               url: "/devotion",
               views: {
               'menuContent' :{
               templateUrl: "templates/devotion.html"
               }
               }
               })
        .state('genius.knowledge', {
               url: "/knowledge",
               views: {
               'menuContent' :{
               templateUrl: "templates/knowledge.html"
               }
               }
               })
        .state('genius.adaptability', {
               url: "/adaptability",
               views: {
               'menuContent' :{
               templateUrl: "templates/adaptability.html"
               }
               }
               })
        .state('genius.chances', {
               url: "/chances",
               views: {
               'menuContent' :{
               templateUrl: "templates/chances.html"
               }
               }
               })
        .state('genius.communicate', {
               url: "/communicate",
               views: {
               'menuContent' :{
               templateUrl: "templates/communicate.html"
               }
               }
               })
        .state('genius.curiosity', {
               url: "/curiosity",
               views: {
               'menuContent' :{
               templateUrl: "templates/curiosity.html"
               }
               }
               })
        .state('genius.honesty', {
               url: "/honesty",
               views: {
               'menuContent' :{
               templateUrl: "templates/honesty.html"
               }
               }
               })
        .state('genius.energy', {
               url: "/energy",
               views: {
               'menuContent' :{
               templateUrl: "templates/energy.html"
               }
               }
               })
        .state('genius.enterprise', {
               url: "/enterprise",
               views: {
               'menuContent' :{
               templateUrl: "templates/enterprise.html"
               }
               }
               })
        .state('genius.enthusiasm', {
               url: "/enthusiasm",
               views: {
               'menuContent' :{
               templateUrl: "templates/enthusiasm.html"
               }
               }
               })
        .state('genius.humor', {
               url: "/humor",
               views: {
               'menuContent' :{
               templateUrl: "templates/humor.html"
               }
               }
               })
        .state('genius.idealism', {
               url: "/idealism",
               views: {
               'menuContent' :{
               templateUrl: "templates/idealism.html"
               }
               }
               })
        .state('genius.imagination', {
               url: "/imagination",
               views: {
               'menuContent' :{
               templateUrl: "templates/imagination.html"
               }
               }
               })
        .state('genius.individualism', {
               url: "/individualism",
               views: {
               'menuContent' :{
               templateUrl: "templates/individualism.html"
               }
               }
               })
        .state('genius.judge', {
               url: "/judge",
               views: {
               'menuContent' :{
               templateUrl: "templates/judge.html"
               }
               }
               })
        .state('genius.optimism', {
               url: "/optimism",
               views: {
               'menuContent' :{
               templateUrl: "templates/optimism.html"
               }
               }
               })
        .state('genius.outgoingness', {
               url: "/outgoingness",
               views: {
               'menuContent' :{
               templateUrl: "templates/outgoingness.html"
               }
               }
               })
        .state('genius.patience', {
               url: "/patience",
               views: {
               'menuContent' :{
               templateUrl: "templates/patience.html"
               }
               }
               })
        .state('genius.perception', {
               url: "/perception",
               views: {
               'menuContent' :{
               templateUrl: "templates/perception.html"
               }
               }
               })
        .state('genius.perfectionism', {
               url: "/perfectionism",
               views: {
               'menuContent' :{
               templateUrl: "templates/perfectionism.html"
               }
               }
               })
        .state('genius.persuasion', {
               url: "/persuasion",
               views: {
               'menuContent' :{
               templateUrl: "templates/persuasion.html"
               }
               }
               })
        .state('genius.versatility', {
               url: "/versatility",
               views: {
               'menuContent' :{
               templateUrl: "templates/versatility.html"
               }
               }
               })
        .state('genius.about', {
               url: "/about",
               views: {
               'menuContent' :{
               templateUrl: "templates/about.html"
               }
               }
               })
        .state('genius.aboutpcyc', {
               url: "/aboutpcyc",
               views: {
               'menuContent' :{
               templateUrl: "templates/aboutpcyc.html"
               }
               }
               })
        .state('genius.howto', {
               url: "/howto",
               views: {
               'menuContent' :{
               templateUrl: "templates/howto.html"
               }
               }
               })
        .state('genius.video', {
               url: "/video",
               views: {
               'menuContent' :{
               templateUrl: "templates/video.html"
               }
               }
               })
         .state('genius.terms2', {
                url: "/terms2",
                views: {
                'menuContent' :{
                templateUrl: "templates/terms2.html"
                }
                }
                })
/*        .state('about', {
               url: "/about",
               templateUrl: "templates/about.html"
               })
        .state('video', {
               url: "/video",
               templateUrl: "templates/video.html"
               })
        .state('genius.attendees', {
               url: "^/attendees",
               views: {
               'menuContent' :{
               templateUrl: "templates/attendees.html",
               controller: "AttendeesCtrl"
               }
               }
               })*/

        $urlRouterProvider.otherwise("/terms");
})

.controller('termsCtrl', function($scope, $rootScope, $location, $localstorage,  $state, $ionicHistory) {

  var initialize = function(){
		// $scope.doRefreshState();
    var terms = localStorage.getItem('terms');
    if(terms != null){
      //console.log('Terms Is Set');
      $location.path("/genius/home");
    }
    else{
      //console.log('Terms Is Not Set');
    }
    //$rootScope.hideSplashScreen();
	}();

  $scope.setTerms = function() {
      localStorage.setItem('terms', true);
      $location.path("/genius/home");
    };


//  var t = $localstorage.get('terms');

/*
  if (typeof tt !== undefined || typeof tt !== null) {
    $state.go('genius.home');
  } else {
    $state.go('terms');
  } */
/*
  $scope.checkTerms = function() {
      var t = $localstorage.getObject('terms');
      var tt = t.terms;
      if (typeof tt !== undefined || typeof tt !== null) {
        $state.go('genius.home');
      } else {
        $state.go('terms');
      }
  };
  $scope.setTerms = function() {
      $localstorage.set('terms', true)
        $state.go('genius.home');

  };
*/
})


.controller('MainCtrl', function($scope, $ionicSideMenuDelegate, $ionicModal, $localstorage, $ionicPopup, $timeout, $ionicLoading, $state, $ionicHistory) {


        /*    $scope.checkTerms = function() {
                var t = $localstorage.getObject('terms');
                var tt = t.terms;
                if (typeof tt !== undefined || typeof tt !== null) {
                  $state.go('genius.home');
                } else {
                  $state.go('terms');
                }
            }; */

            $scope.navi = function(x) {
            $scope.loadingIndicator = $ionicLoading.show({
                                                         content: '<i class="icon ion-loading-c"></i>',
                                                         animation: 'fade-in',
                                                         showBackdrop: true,
                                                         maxWidth: 200,
                                                         showDelay: 5 	// If the delay is too fast and you also change states, while the loader is showing, you can get flashing behavior
                                                         });
            $timeout(function() {
                     $state.go('genius.'+x);
                     }, 500);
            // Hide the loadingIndicator 1500 ms later
            $timeout(function(){
                     $ionicLoading.hide();
                     }, 1500);
            };


            $scope.showSaving = function() {
                var myPopup = $ionicPopup.show({
                                           title: 'Please Wait',
                                           template: 'Saving...',
                                           scope: $scope
                                           });
                $timeout(function() {
                     myPopup.close();
                     }, 3000);
            };
            $scope.showEmptyNote = function() {
            var myPopup2 = $ionicPopup.show({
                                           title: 'Error',
                                           template: 'Can\'t save an empty note.',
                                           scope: $scope
                                           });
            $timeout(function() {
                     myPopup2.close();
                     }, 3000);
            };

            $scope.history = {
                drive:  "",
                genius: "",
                adaptability: "",
                takechances: "",
                communicate: "",
                courage: "",
                curiosity: "",
                devotion: "",
                energy: "",
                enterprise: "",
                enthusiasm: "",
                honesty: "",
                humor: "",
                idealism: "",
                imagination: "",
                individualism: "",
                judge: "",
                knowledge: "",
                optimism: "",
                outgoingness: "",
                patience: "",
                perception: "",
                perfectionism: "",
                persuasion: "",
                versatility: ""
            };
            $scope.status = {
            drive:  "",
            genius: "",
            adaptability: "",
            takechances: "",
            communicate: "",
            courage: "",
            curiosity: "",
            devotion: "",
            energy: "",
            enterprise: "",
            enthusiasm: "",
            honesty: "",
            humor: "",
            idealism: "",
            imagination: "",
            individualism: "",
            judge: "",
            knowledge: "",
            optimism: "",
            outgoingness: "",
            patience: "",
            perception: "",
            perfectionism: "",
            persuasion: "",
            versatility: ""
            };



            $ionicSideMenuDelegate.canDragContent(false);

            $scope.toggleLeft = function() {
                $ionicSideMenuDelegate.toggleLeft();
            };

            $scope.transIn = function(x, y) {
                kendo.fx($("#flip-"+x)).flip("horizontal", $("#"+y+""), $("#"+x+"-def")).play();
            };
            $scope.transOut = function(x, y) {
                kendo.fx($("#flip-"+x)).flip("horizontal", $("#"+x+"-def"), $("#"+y+"")).play();
            };

            $scope.getCounts = function(item, array) {
                var count = 0;
                $.each(array, function(i,v) { if (v === item) count++; });
                return count;
            };

            $scope.goBack = function() {
              $ionicHistory.goBack();
            };

// SAVE NOTE
        $scope.saveNote = function (x) {
            $scope.showSaving();
            //$('.noteform').find('[data-role]').attr('disabled','disabled'); //disable button so it cant be double clicked if the device lags
            //console.log($("textarea."+x).val())
            //if ($("textarea."+x).val() == "") { //showPopUpMessage('<p>Can\'t save an empty note.</p>');
            //    $scope.showEmptyNote();
            //    return false;
            //} else {
            //$("#status-"+x).focus();
            var notesdata = '('+kendo.toString(new Date($.now()), "g")+') '+$("textarea."+x).val();
            var notesnew = $("textarea."+x).val();
            var notestatus = $('input[name='+x+']:checked').val();
            var noteshistory = $("textarea."+x+"history").val();
            var noteshistoryEl = $("textarea."+x+"history");
            //var charsObj = JSON.parse(window.localStorage.getItem('notes'));
            var charsObj = $localstorage.getObject('notes')
            var history = charsObj[x].history;


            //if empty note just save status else save all.
            if (notesnew == "") {
                charsObj[x].status = notestatus;
            } else {
                charsObj[x].data = notesdata;
                charsObj[x].status = notestatus;
                charsObj[x].history = notesdata+"\n"+noteshistory;
            }

            $localstorage.setObject('notes', charsObj)

            //window.localStorage.setItem('notes', JSON.stringify(charsObj));
            //$('div#status-'+x).html('Note Saved').fadeIn('slow').delay(2000).fadeOut('slow');
            //showPopUpMessage('<p>Note Saved</p>');
            $scope.getNote(x);
            $scope.getStatus();

            //$('body').scrollTop(0);
            //$('.noteform').find('[data-role]').removeAttr('disabled','disabled');  //reenable the save button now that data has been saved.
            //}
        };
// GET COUNT OF ITEMS IN ARRAY
        $scope.getCounts = function (item,array) {

            var count = 0;
            $.each(array, function(i,v) { if (v === item) count++; });
            return count;

        };

//get the note data when its loaded(view changes). Except for home because there is nothing to load for homepage
// FOR IOS: when homepage loads check localstorage for data since it dont do it on app load.
            $scope.$on('$stateChangeSuccess',
                           function(event, toState, toParams, fromState, fromParams){
                            var str = toState.url.substring(1,(toState.url.length));  //strip the leading / from the url.
                            if (str !== "home")  { $scope.getNote(str);  }
                            //if (str == "about" || str == "aboutpcyc")  {  }
                           })

 // GET NOTE
            $scope.getNote = function (x) {
              // dont get notes for pages that dont have them.
            if (typeof x !== 'undefined' && x !== "home" && x !== "howto" && x !== "about" && x !== "aboutpcyc" && x !== "terms" && x !== "terms2") {

                var notesdiv = $("textarea.notes."+x); var noteshist = $("textarea.notes."+x+"history");
                //var charsObj = JSON.parse(localStorage.getItem('notes'));
                var charsObj = $localstorage.getObject('notes');

                    var data = charsObj[x].data;
                    var status = charsObj[x].status;
                    var history = charsObj[x].history;

                notesdiv.val("");
                //noteshist.val(history);
                $scope.status[x] = status;
                $scope.history[x] = history;
                //$('input[name="'+x+'"][value="'+status+'"]').prop('checked', true);

            }
            }


// GET STATUS
            $scope.getStatus = function () {

            var chars = ["drive","courage","devotion","knowledge","honesty","optimism","judge","enthusiasm","takechances","energy","enterprise","persuasion","outgoingness","communicate","patience","perception","perfectionism","humor","versatility","adaptability","curiosity","individualism","idealism","imagination"];

            var status = ""; var statusArr = []; var colors = {notstarted:"red", inprogress:"yellow",completed:"green"};
            var charsObj = JSON.parse(localStorage.getItem('notes'));
            $('style').html();

            var c = 0;
            $.each(charsObj, function( i, v ) {

                   status = v.status;

                   if (i !== "notes") {
                   if (status !== null) {
                     if (status == "inprogress") {
                     $('style').append("div[data-badge='"+(c)+"']:after{background:"+colors[status]+"; color: #000 }");
                     } else {
                     $('style').append("div[data-badge='"+(c)+"']:after{background:"+colors[status]+"}");
                     }
                   statusArr.push(status);
                   } else {
                   //console.log(status)
                   // localStorage.setItem('note-'+chars[i]+'-status', "notstarted");
                   statusArr.push('notstarted');
                   }
                   c++;
                   }
                   });
            //console.log(statusArr)
            var a = $scope.getCounts('notstarted',statusArr);
            var b = $scope.getCounts('inprogress',statusArr);
            var c = $scope.getCounts('completed',statusArr);
            // set status and update chart
            $scope.status.data[0].value = a;
            $scope.status.data[1].value = b;
            $scope.status.data[2].value = c;

            };


            $scope.pie = {
            title: {
            visible: false
            },
            legend: {
            position: "top",
                labels: {
                    color: "#FFFFFF"
                }
            },
            chartArea: {
            background: ""
            },
            seriesDefaults: {
            labels: {
            visible: true,
            position: "insideEnd",
            background: "transparent",
            //template: "#= category #: #= value#"
            template: "#= value#"
            }
            },
            series: [{
                     type: "pie",
                     field: "value",
                     categoryField: "category"
                     }]
            };

            $scope.status = {
            data: [{
                   category: "Not Started",
                   value: "25",
                   color: "red"
                   }, {
                   category: "In Progress",
                   value: "0",
                   color: "#F0F000"
                   }, {
                   category: "Completed",
                   value: "0",
                   color: "green"
                   }]
            };



// DEFINE MODALS
            // Modal 1
            $ionicModal.fromTemplateUrl('modals/genius.html', {
                                        id: '1',
                                        scope: $scope,
                                        animation: 'slide-in-up'
                                        }).then(function(modal) {
                                                $scope.modalGenius = modal;
                                                });

            // Modal 2
            $ionicModal.fromTemplateUrl('modals/drive.html', {
                                        id: '2',
                                        scope: $scope,
                                        animation: 'slide-in-up'
                                        }).then(function(modal) {
                                                $scope.modalDrive = modal;
                                                });

            $ionicModal.fromTemplateUrl('modals/courage.html', {
                                        id: '3',
                                        scope: $scope,
                                        animation: 'slide-in-up'
                                        }).then(function(modal) {
                                                $scope.modalCourage = modal;
                                                });
            $ionicModal.fromTemplateUrl('modals/judge.html', {
                                        id: '4',
                                        scope: $scope,
                                        animation: 'slide-in-up'
                                        }).then(function(modal) {
                                                $scope.modalJudge = modal;
                                                });
            $ionicModal.fromTemplateUrl('modals/optimism.html', {
                                        id: '5',
                                        scope: $scope,
                                        animation: 'slide-in-up'
                                        }).then(function(modal) {
                                                $scope.modalOptimism = modal;
                                                });
            $ionicModal.fromTemplateUrl('modals/chances.html', {
                                        id: '6',
                                        scope: $scope,
                                        animation: 'slide-in-up'
                                        }).then(function(modal) {
                                                $scope.modalTakechances = modal;
                                                });
            $ionicModal.fromTemplateUrl('modals/energy.html', {
                                        id: '7',
                                        scope: $scope,
                                        animation: 'slide-in-up'
                                        }).then(function(modal) {
                                                $scope.modalEnergy = modal;
                                                });
            $ionicModal.fromTemplateUrl('modals/persuasion.html', {
                                        id: '8',
                                        scope: $scope,
                                        animation: 'slide-in-up'
                                        }).then(function(modal) {
                                                $scope.modalPersuasion = modal;
                                                });
            $ionicModal.fromTemplateUrl('modals/outgoingness.html', {
                                        id: '9',
                                        scope: $scope,
                                        animation: 'slide-in-up'
                                        }).then(function(modal) {
                                                $scope.modalOutgoingness = modal;
                                                });
            $ionicModal.fromTemplateUrl('modals/imagination.html', {
                                        id: '10',
                                        scope: $scope,
                                        animation: 'slide-in-up'
                                        }).then(function(modal) {
                                                $scope.modalImagination = modal;
                                                });
            $ionicModal.fromTemplateUrl('modals/curiosity.html', {
                                        id: '11',
                                        scope: $scope,
                                        animation: 'slide-in-up'
                                        }).then(function(modal) {
                                                $scope.modalCuriosity = modal;
                                                });




/* Listen for broadcasted messages */

            $scope.$on('modal.shown', function(event, modal) {
                       //console.log('Modal ' + modal.id + ' is shown!');
                       });

            $scope.$on('modal.hidden', function(event, modal) {
                       //console.log('Modal ' + modal.id + ' is hidden!');
                       });

            // Cleanup the modals when we're done with them (i.e: state change)
            // Angular will broadcast a $destroy event just before tearing down a scope
            // and removing the scope from its parent.
            $scope.$on('$destroy', function() {
                       //console.log('Destroying modals...');
                       $scope.modalGenius.remove();
                       $scope.modalDrive.remove();
                       $scope.modalCourage.remove();
                       $scope.modalJudge.remove();
                       $scope.modalOptimism.remove();
                       $scope.modalTakechances.remove();
                       $scope.modalEnergy.remove();
                       $scope.modalPersuasion.remove();
                       $scope.modalOutgoingness.remove();
                       $scope.modalImagination.remove();
                       $scope.modalCuriosity.remove();
                       });



// APP INIT

            var init = function () {
            // check for localstorage data. if not set it to blank
                var isData = $localstorage.getObject('notes');

            // if no data then set it
            if (!isData.hasOwnProperty('notes')) {

            $localstorage.setObject('notes', {"genius":{"status":"notstarted","data":"","history":""},"drive":{"status":"notstarted","data":"","history":""},"courage":{"status":"notstarted","data":"","history":""},"devotion":{"status":"notstarted","data":"","history":""},"knowledge":{"status":"notstarted","data":"","history":""},"honesty":{"status":"notstarted","data":"","history":""},"optimism":{"status":"notstarted","data":"","history":""},"judge":{"status":"notstarted","data":"","history":""},"enthusiasm":{"status":"notstarted","data":"","history":""},"takechances":{"status":"notstarted","data":"","history":""},"energy":{"status":"notstarted","data":"","history":""},"enterprise":{"status":"notstarted","data":"","history":""},"persuasion":{"status":"notstarted","data":"","history":""},"outgoingness":{"status":"notstarted","data":"","history":""},"communicate":{"status":"notstarted","data":"","history":""},"patience":{"status":"notstarted","data":"","history":""},"perception":{"status":"notstarted","data":"","history":""},"perfectionism":{"status":"notstarted","data":"","history":""},"humor":{"status":"notstarted","data":"","history":""},"versatility":{"status":"notstarted","data":"","history":""},"adaptability":{"status":"notstarted","data":"","history":""},"curiosity":{"status":"notstarted","data":"","history":""},"individualism":{"status":"notstarted","data":"","history":""},"idealism":{"status":"notstarted","data":"","history":""},"imagination":{"status":"notstarted","data":"","history":""},"notes":{"status":"notstarted","data":"","history":""}});

            }
            //once localstorage is set/checked. get statuses
                $scope.getStatus();



            };
            // and fire init after definition
            init();

})
// this is used to load the HTML5 video on the video page.
.directive('dynamicUrl', [function () {
                          return {
                          restrict: 'A',
                          link: function postLink(scope, element, attr) {

                              element.attr('src', attr.dynamicUrlSrc);
                          //console.log(attr)
                          //element.find('video').removeAttr( "autoplay" )
                          }
                          }
                          }])
