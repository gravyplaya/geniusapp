#!/bin/bash
## a script for generating signed android release builds

## CONSTANTS
RIGHT_NOW=$(date +"%m%d_%H%M%S")
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
#path to where 'cordova build android' command builds to
ANDROID_BUILD_PATH=$SCRIPTPATH/platforms/android/ant-build


echo "building android release package"
cordova build --release android
echo "signing package"
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $ANDROID_BUILD_PATH/genius-release-key.keystore -storepass "Just4now!" $ANDROID_BUILD_PATH/genius2-release-unsigned.apk genius
echo "optimizing package"
zipalign -v -f 4 $ANDROID_BUILD_PATH/genius2-release-unsigned.apk $ANDROID_BUILD_PATH/genius2-release-signed.apk
echo "saving apk"
cp $ANDROID_BUILD_PATH/genius2-release-signed.apk $SCRIPTPATH/genius_beta_$RIGHT_NOW.apk
echo "saved as $SCRIPTPATH/genius_beta_$RIGHT_NOW.apk"